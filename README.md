# NodeJS

# Problem Statement:
Create a NodeJS application that acts as a simple REST API for managing a collection of books. The API should allow users to perform CRUD operations (Create, Read, Update, Delete) on the books.

Each book should have the following properties:
• id (unique identifier for the book)
• title (title of the book)
• author (author of the book)
• price (price of the book)

The API should support the following endpoints:
1. GET /books: Retrieve a list of all books.
2. GET /books/{id}: Retrieve the details of a specific book by its ID.
3. POST /books: Create a new book.
4. PUT /books/{id}: Update the details of a specific book by its ID.
5. DELETE /books/{id}: Delete a specific book by its ID.
6. Use an in-memory data structure (e.g., an array or object) to store the 
books.

Requirements:
1. Use NodeJS and Express.js to build the REST API.
2. Use appropriate HTTP status codes and JSON responses.
3. Use appropriate error handling and validation.
4. Use a separate module or file to handle the book data.
5. Use nodemon or a similar tool for automatic server restarts during 
development.
6. Add pagination and sorting options to the book list endpoint.
7. Implement additional search/filtering functionality (e.g., by author, price range, etc.).
8. Write unit tests to ensure the correctness of the API endpoints.
9. Remember to install the required dependencies using npm or yarn before 
starting the development.
10. Add input validation for the book properties.

# Bonus points:
1. Implement data persistence using a JSON file or a database.