const { v4 } = require('uuid')
const validateSchema = require('../validator/bookModel.js')
const bookService = require('../services/index.js')

const getAllBooks = function (req, res, next) {
  try {
    const allBooks = bookService.getAllBooks()
    return res.status(200).json(allBooks)
  } catch (error) {
    next(error)
  }
}

const getBookById = function (req, res, next) {
  const bookId = req.params.id
  try {
    const book = bookService.findBook(bookId)
    if (book) {
      return res.status(200).json(book)
    } else {
      return res.status(400).json({ message: 'No book exists with the id' })
    }
  } catch (error) {
    next(error)
  }
}

const createNewBook = function (req, res, next) {
  const bookData = req.body
  try {
    // Validate the book details
    bookData.id = v4()
    const { error, value } = validateSchema(bookData)
    if (error) {
      return res.status(400).json({ message: error.details[0].message })
    }
    bookService.addBook(value)
    return res.status(201).json(value)
  } catch (error) {
    next(error)
  }
}

const updateBook = function (req, res, next) {
  const bookId = req.params.id
  const bookData = req.body
  try {
    const bookIndex = bookService.findBookIndex(bookId)
    if (bookIndex !== -1) {
      bookData.id = bookId
      const { error, value } = validateSchema(bookData)
      if (error) {
        return res.status(400).json(error.details[0].message)
      } else {
        const updatedBook = bookService.updateBookData(bookIndex, value)
        return res.status(200).json(updatedBook)
      }
    } else {
      return res.status(400).json({ message: 'Book does not exist with given id' })
    }
  } catch (error) {
    next(error)
  }
}

const deleteBook = function (req, res, next) {
  const bookId = req.params.id
  try {
    const bookIndex = bookService.findBookIndex(bookId)
    if (bookIndex !== -1) {
      bookService.deleteBook(bookIndex)
      return res.status(200).json({ message: 'Deleted successfully' })
    } else {
      return res.status(400).json({ message: 'Book does not exist with given id' })
    }
  } catch (error) {
    next(error)
  }
}

const filterBooks = function (req, res, next) {
  const queryParams = req.query
  try {
    const reqBooks = bookService.filterBooks(queryParams)
    return res.status(200).json(reqBooks)
  } catch (error) {
    next(error)
  }
}

module.exports = {
  getAllBooks,
  getBookById,
  createNewBook,
  updateBook,
  deleteBook,
  filterBooks
}
