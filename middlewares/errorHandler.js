function errorHandler (err, req, res, next) {
  if (err) {
    return res.status(400).json({
      error: {
        // message:"Something went wrong...!"
        message: err.message
      }
    })
  }
}

module.exports = errorHandler
