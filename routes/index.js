const express = require('express')
const bookService = require('../controllers/index.js')
const errorHandler = require('../middlewares/errorHandler.js')

const router = express.Router()

router.get('/getAllBooks', bookService.getAllBooks)
router.get('/getBookById/:id', bookService.getBookById)
router.post('/createNewBook', bookService.createNewBook)
router.put('/updateBook/:id', bookService.updateBook)
router.delete('/deleteBook/:id', bookService.deleteBook)
router.get('/filter', bookService.filterBooks)

router.use(errorHandler)

module.exports = router
