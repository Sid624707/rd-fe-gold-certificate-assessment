const fs = require('fs')

function writeToFile (bookData) {
  const jsonData = JSON.stringify(bookData, null, 2)
  fs.writeFileSync('books.json', jsonData)
}

function readFromFile () {
  const data = fs.readFileSync('books.json')
  return JSON.parse(data)
}

const getAllBooks = () => {
  const jsonBooks = readFromFile()
  return jsonBooks
}

const findBook = (bookId) => {
  const jsonBooks = readFromFile()
  return jsonBooks.find((book) => {
    return book.id === bookId
  })
}

const updateBookData = (bookIndex, value) => {
  const jsonBooks = readFromFile()
  const updatedBook = { ...value }
  jsonBooks[bookIndex] = updatedBook
  writeToFile(jsonBooks)
  return updatedBook
}

const findBookIndex = (bookId) => {
  const jsonBooks = readFromFile()
  return jsonBooks.findIndex((book) => {
    return book.id === bookId
  })
}

const addBook = (book) => {
  const jsonBooks = readFromFile()
  jsonBooks.push(book)
  writeToFile(jsonBooks)
}

const deleteBook = (index) => {
  const jsonBooks = readFromFile()
  jsonBooks.splice(index, 1)
  writeToFile(jsonBooks)
}

const filterBooks = (queryParams) => {
  let reqBooks = readFromFile()
  const { author, title, sort, page, limit = 2 } = queryParams
  if (author) {
    reqBooks = filterBooksByAuthor(reqBooks, author)
  }
  if (title) {
    reqBooks = filterBooksByTitle(reqBooks, title)
  }
  if (sort) {
    if (sort === 'price') {
      reqBooks = reqBooks.sort(sortBooksByPrice)
    }
    if (sort === 'title') {
      reqBooks = reqBooks.sort(sortBooksByTitle)
    }
    if (sort === 'author') {
      reqBooks = reqBooks.sort(sortBooksByAuthor)
    }
  }
  if (page) {
    const start = (page - 1) * limit
    const end = (page) * limit
    reqBooks = reqBooks.slice(start, end)
  }
  return reqBooks
}

const filterBooksByAuthor = (books, author) => {
  return books.filter((book) => {
    return book.author === author
  })
}

const filterBooksByTitle = (books, bookName) => {
  return books.filter((book) => {
    return book.title === bookName
  })
}

const sortBooksByTitle = (book1, book2) => {
  return book1.title > book2.title ? 1 : -1
}

const sortBooksByAuthor = (book1, book2) => {
  return book1.author > book2.author ? 1 : -1
}

const sortBooksByPrice = (book1, book2) => {
  return book1.price - book2.price
}

module.exports = {
  getAllBooks,
  findBook,
  addBook,
  findBookIndex,
  deleteBook,
  filterBooksByAuthor,
  filterBooksByTitle,
  filterBooks,
  readFromFile,
  writeToFile,
  updateBookData
}
