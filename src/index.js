const express = require('express')
const bookRoutes = require('../routes/index')

const app = express()

app.use(express.json())
app.use('/books', bookRoutes)

const PORT = process.env.PORT || 9876

app.get('/', function (req, res) {
  res.send('Hi, This is an Assesment on Node.js')
})

app.listen(PORT, function () {
  console.log(`Listening on http://localhost:${PORT}`)
})

module.exports = app
