const chai = require('chai')
const { describe, it, before } = require('mocha')
const chaiHttp = require('chai-http')
const server = require('../src/index')
const { writeToFile } = require('../services')

chai.use(chaiHttp)

// Assertion Style
chai.should()

describe('Testing Books API', () => {
  before(() => {
    const data = [
      {
        title: 'Fault In Our stars',
        author: 'John S Green',
        price: 987,
        id: '0ce2ff76-aa85-4cc8-8fc7-fbba2afeef9d'
      },
      {
        title: 'Time for a Change',
        author: 'Erica James',
        price: 874,
        id: '0762ad1c-0d4c-4574-b2eb-759af5ca940d'
      },
      {
        title: 'Like a Flowing River',
        author: 'Paulo Coelho',
        price: 567,
        id: 'eaf3d396-ff62-48d8-bc70-eea6673450af'
      },
      {
        title: 'Harry Potter',
        author: 'J.K.Rowling',
        price: 1023,
        id: 'a1afa88c-a9e4-4832-86f4-f0e1175ee9ff'
      }
    ]
    writeToFile(data)
  })

  describe('Test GET /books', function () {
    it('should get all the books', function (done) {
      chai.request(server)
        .get('/books/getAllBooks')
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(200)
          res.body.should.be.a('array')
          res.body[0].should.should.be.a('object')
          done()
        })
    })
  })

  describe('Test GET /books/:id', function () {
    it('should get the book with valid id', function (done) {
      const bookId = '0762ad1c-0d4c-4574-b2eb-759af5ca940d'
      chai.request(server)
        .get('/books/getBookById/' + bookId)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('id').eq(bookId)
          res.body.should.have.property('title').eq('Time for a Change')
          res.body.should.have.property('author').eq('Erica James')
          res.body.should.have.property('price').eq(874)
          done()
        })
    })

    it('should not get the book with invalid id', function (done) {
      const bookId = '30cc908f-f70a-4371-9ab8-6bd8cce4f864'
      chai.request(server)
        .get('/books/getBookById/' + bookId)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('message').eq('No book exists with the id')
          done()
        })
    })
  })

  describe('Test POST /books', function () {
    it('should create a new book', function (done) {
      const newBook = {
        title: 'Testing',
        author: 'Alfred',
        price: 234
      }
      chai.request(server)
        .post('/books/createNewBook')
        .send(newBook)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(201)
          res.body.should.be.a('object')
          res.body.should.have.property('title').eq('Testing')
          res.body.should.have.property('author').eq('Alfred')
          res.body.should.have.property('price').eq(234)
          done()
        })
    })

    it('should not create a new book with title less than 3 charcters', function (done) {
      const newBook = {
        title: 'Te',
        author: 'Alfred',
        price: 234
      }
      chai.request(server)
        .post('/books/createNewBook')
        .send(newBook)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('message').eq('"title" length must be at least 3 characters long')
          done()
        })
    })

    it('should not create a new book with author name less than 3 charcters', function (done) {
      const newBook = {
        title: 'Testing',
        author: 'Al',
        price: 234
      }
      chai.request(server)
        .post('/books/createNewBook')
        .send(newBook)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('message').eq('"author" length must be at least 3 characters long')
          done()
        })
    })

    it('should not create a new book when price is not a number', function (done) {
      const newBook = {
        title: 'Testing',
        author: 'Alfred',
        price: 'abc'
      }
      chai.request(server)
        .post('/books/createNewBook')
        .send(newBook)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('message').eq('"price" must be a number')
          done()
        })
    })
  })

  describe('Test PUT /books/:id', function () {
    it('should update the user with valid id', function (done) {
      const bookId = '0762ad1c-0d4c-4574-b2eb-759af5ca940d'
      const newBook = {
        title: 'Time for a new Change',
        author: 'Erica H James',
        price: 2348
      }
      chai.request(server)
        .put('/books/updateBook/' + bookId)
        .send(newBook)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('id').eq(bookId)
          res.body.should.have.property('title').eq('Time for a new Change')
          res.body.should.have.property('author').eq('Erica H James')
          res.body.should.have.property('price').eq(2348)
          done()
        })
    })

    it('should not update the book with invalid id', function (done) {
      const bookId = '30cc908f-f70a-4371-9ab8-6bd8cce4f864'
      const newBook = {
        title: 'Time for a new Change',
        author: 'Erica H James',
        price: 2348
      }
      chai.request(server)
        .put('/books/updateBook/' + bookId)
        .send(newBook)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('message').eq('Book does not exist with given id')
          done()
        })
    })
  })

  describe('Test DELETE /books/:id', function () {
    it('should delete the user with valid id', function (done) {
      const bookId = '0762ad1c-0d4c-4574-b2eb-759af5ca940d'
      chai.request(server)
        .delete('/books/deleteBook/' + bookId)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('message').eq('Deleted successfully')
          done()
        })
    })

    it('should not delete the book with invalid id', function (done) {
      const bookId = '30cc908f-f70a-4371-9ab8-6bd8cce4f8667'
      chai.request(server)
        .delete('/books/deleteBook/' + bookId)
        .end(function (err, res) {
          if (err) {
            console.log(err)
          }
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('message').eq('Book does not exist with given id')
          done()
        })
    })
  })
})
