const Joi = require('joi')

const validator = (schema) => {
  return function (payload) {
    return schema.validate(payload)
  }
}

const bookSchema = Joi.object({
  id: Joi.string().required(),
  title: Joi.string().min(3).required(),
  author: Joi.string().min(3).required(),
  price: Joi.number().required()
})

const validateSchema = validator(bookSchema)
module.exports = validateSchema
